var app = angular.module('esport', []);
app.controller('termsController', function($scope,jsonloader,$http,$window){ 
	
	$scope.regulationsAccepted = false;
	$scope.message = false;
	$scope.booking = { "game": "Nie wybrano gry" };
	$scope.participants = {};
	$scope.dates = {};
	$scope.debug = false;
	$scope.messageSent = false;
	
	jsonloader.getParticipants().then(function(msg){
		$scope.participants = msg.data.participants;
	});
	jsonloader.getDates().then(function(msg){
		$scope.dates = msg.data.dates;
	});
	
	$scope.registrationStatus = function(){
		var now = new Date().getTime();
		var regStart = $scope.dates.registrationOpen;
		var regEnd = $scope.dates.registrationCloses;
		if($scope.debug){
			return 1;	
		} else {
		if(now < regStart ) {
			return 0;
		}
		if(now > regStart && now < regEnd){
			return 1;
		}
		if(now > regEnd){
			return 2;
		}
		}
	};
	
	
	$scope.handleFormSubmit = function () {
		var booking = $scope.booking;
		    $http.post('action.php', booking).success(function (data, status) {
		    	$scope.booking = {};
		    }).error(function (data, status) {
		    	$scope.booking = {};
		    	$scope.messageSent = true;
		        $window.alert("Dziękujemy za zgłoszenie!\nProsimy oczekiwać na e-mail zwrotny od organizatora.");
		    });  
		};

});

