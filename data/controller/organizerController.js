var app = angular.module('esport', []);
app.controller('organizerController', function($scope,jsonloader){ 
	
	$scope.organizers = {};
	
	jsonloader.getOrganizers().then(function(msg){
		$scope.organizers = msg.data.organizers;
	});
	
	$scope.getRandomStyle = function(){
		var number = Math.floor((Math.random() * 5) + 1);
		return number;
	};
	
});

