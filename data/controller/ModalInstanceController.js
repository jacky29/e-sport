angular
.module('esport')
.controller('ModalInstanceController', ['$scope', '$uibModalInstance', 'object', ModalInstanceController] );

function ModalInstanceController($scope,$uibModalInstance,object){

	$scope.object = object;
	
	$scope.confirm = function () {
		$uibModalInstance.close;
	};
	
	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};	
}