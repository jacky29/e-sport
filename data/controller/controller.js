var app = angular.module('esport', ['angular-timeline']);
app.controller('mainController', function($scope,jsonloader){ 
	
	$scope.website = {};
	$scope.sponsors = {};
	$scope.showMore = false;
	$scope.loadStream = false;
	$scope.events = {};
	$scope.dates = {};
	
	$scope.streamAvailable = function(){
		var dateTime = new Date().getTime();
		if(dateTime >= $scope.dates.streamStart && dateTime < $scope.dates.streamEnds){
			return true;
		} else {
			return false;
		}
	};
	
	jsonloader.getWebsite().then(function(msg){
		$scope.website = msg.data.webpage;
	});
	jsonloader.getSponsors().then(function(msg){
		$scope.sponsors = msg.data.sponsors;
	});
	jsonloader.getEvents().then(function(msg){
		$scope.events = msg.data.schedule;
	});
	jsonloader.getDates().then(function(msg){
		$scope.dates = msg.data.dates;
	});
	
	$scope.showStream = function(value){
		$scope.loadStream = value;
	};

});

