angular
.module('esport')
.factory('jsonloader', function ($http) {
    return {
        getWebsite: function () {
            return $http.get('data/json/webpage.json');
        },
        getOrganizers: function () {
            return $http.get('data/json/organizers.json');
        },
        getSponsors: function () {
        	return $http.get('data/json/sponsors.json');
        },
        getEvents: function () {
        	return $http.get('data/json/schedule.json');
        },
        getParticipants: function(){
        	return $http.get('data/json/participants.json');
        },
        getDates: function(){
        	return $http.get('data/json/dates.json');
        }
    };
});
